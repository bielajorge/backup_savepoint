export class User {
    constructor(
        public email: string,
        public name: string,
        public password: string) { }

    matches(another: User): boolean {
        return another !== undefined
            && another.email === this.email
            && another.password === this.password
    }
}

export const users = {
    "j": new User('j', 'j', '13'),
    "jorge@gmail.com": new User('jorge@gmail.com', 'jorge', '1303')
}