class Order {
    constructor(
        public id: string,
        public nomeRazao: string,
        public CNPJ: string,
        public email?: string,
        public numero?: number,
        public data?: number,
        public dias?: string,
        public dataLiberado?: Date,
        public dataBloqueio?: Date,
        public imagePath?: string,

        public orderItems: OrderItem[] = [],
    ) { }

}

class OrderItem {
    constructor(public quantity: number, public menuId: string) {

    }
}
export { Order, OrderItem }
