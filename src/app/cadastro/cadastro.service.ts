import { ControleService } from "../painel-controle/controle.service";
import { ControleItem } from "../painel-controle/controle.model";
import { Injectable } from "@angular/core";

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

import { Order } from "./cadastro.model";
import { MEAT_API } from "../app.api";
import { LoginService } from '../security/login/login.service';


@Injectable()
export class CadastroService {

  private customersUrl = 'http://localhost:8080/api/customers';

  constructor(
    private controlService: ControleService,
    private http: HttpClient) {
  }

  itemsValue(): number {
    return this.controlService.total()
  }

  cartItems(): ControleItem[] {
    return this.controlService.empresas
  }

  increaseQty(item: ControleItem) {
    this.controlService.increaseQty(item)
  }

  decreaseQty(item: ControleItem) {
    this.controlService.decreaseQty(item)
  }

  remove(item: ControleItem) {
    this.controlService.removeItem(item)
  }

  clear() {
    this.controlService.clear()
  }

  checkOrder(order: Order): Observable<string> {
    return this.http.post<Order>(`${MEAT_API}/empresas`, order)
      .pipe(map(order => order.id))
  }

}