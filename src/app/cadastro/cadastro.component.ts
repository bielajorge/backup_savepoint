import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NotificationService } from '../shared/messages/notification.service';
import { Order, OrderItem } from './cadastro.model';
import { CadastroService } from './cadastro.service';
import { ControleItem } from '../painel-controle/controle.model';
import { Empresa } from '../empresas/empresa/empresa.model';



@Component({
  selector: 'mt-cadastro',
  templateUrl: './cadastro.component.html'
})
export class CadastroComponent implements OnInit {

  delivery: number = 8

  orderForm: FormGroup
  emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  numberPatter = /^[0-9]*$/

  submitted = false;
  empresa: Empresa;

  constructor(
    private orderService: CadastroService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
  ) { }

  itemsValue(): number {
    return this.orderService.itemsValue();
  }

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      nomeRazao: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
      email: this.formBuilder.control('', [Validators.pattern(this.emailPattern)]),
      CNPJ: this.formBuilder.control('', [Validators.required, Validators.pattern(this.numberPatter), Validators.minLength(14)]),
      numero: this.formBuilder.control('', [Validators.pattern(this.numberPatter)]),
      imagePath : this.formBuilder.control('', []),
    })
  }

  cartItems(): ControleItem[] {
    return this.orderService.cartItems()
  }

  increaseQty(item: ControleItem) {
    this.orderService.increaseQty(item)
  }

  decreaseQty(item: ControleItem) {
    this.orderService.decreaseQty(item)
  }

  remove(item: ControleItem) {
    this.orderService.remove(item)
  }

  addEmpresa(order: Order) {
    console.log(order)
    order.orderItems = this.cartItems()
      .map((item: ControleItem) => new OrderItem(item.quantity, item.menuItem.id))

    this.orderService.checkOrder(order)
      .subscribe((orderId: string) => {
        this.notificationService.notify(`Cadastrado com sucesso, ${order.nomeRazao}`),
          this.orderService.clear()
      })
  }
}