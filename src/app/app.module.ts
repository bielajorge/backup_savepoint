import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy, registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt, 'pt')

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlTooltipModule, OwlInputMaskModule } from 'owl-ng';


import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { InicioComponent } from './inicio/inicio.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { EmpresaComponent } from './empresas/empresa/empresa.component'
import { EmpresasService } from './empresas/empresas.service';
import { RestaurantsDetailComponent } from './empresa-detail/restaurants-detail.component';
import { MenuItemComponent } from './empresa-detail/menu-item/menu-item.component';
import { ReviewsComponent } from './empresa-detail/reviews/reviews.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { CadastroService } from './cadastro/cadastro.service';
import { SharedModule } from './shared/shared.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { ControleComponent } from './painel-controle/controle.component';
import { ControleService } from './painel-controle/controle.service';
import { LoginComponent } from './security/login/login.component';
import { UserDetailComponent } from './header/user-detail/user-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InicioComponent,
    EmpresasComponent,
    EmpresaComponent,
    RestaurantsDetailComponent,
    MenuItemComponent,
    ReviewsComponent,
    ControleComponent,
    CadastroComponent,
    NotFoundComponent,
    LoginComponent,
    UserDetailComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlTooltipModule,
    OwlInputMaskModule,
    HttpClientModule,
    SharedModule.forRoot(),
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    EmpresasService,
    ControleService,
    CadastroService,
    { provide: LOCALE_ID, useValue: 'pt' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
