import { Component, OnInit } from '@angular/core';
import { EmpresasService } from '../empresas/empresas.service';
import { Empresa } from '../empresas/empresa/empresa.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'mt-restaurants-detail',
  templateUrl: './restaurants-detail.component.html'
})
export class RestaurantsDetailComponent implements OnInit {

  empresas: Empresa
  
  constructor(private empresasService: EmpresasService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.empresasService.empresaById(this.route.snapshot.params['id'])
      .subscribe(empresa => this.empresas = empresa)
  }

}
