import { Component, OnInit } from '@angular/core';
import { EmpresasService } from '../../empresas/empresas.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'mt-reviews',
  templateUrl: './reviews.component.html'
})
export class ReviewsComponent implements OnInit {

  reviews: Observable<any>
  constructor(private restauranteService: EmpresasService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.reviews = this.restauranteService.reviewsOfEmpresa(this.route.parent.snapshot.params['id'])
  }

}
