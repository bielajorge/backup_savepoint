import { Component, OnInit } from '@angular/core';
import { Empresa } from './empresa/empresa.model';
import { EmpresasService } from './empresas.service';

import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

import { switchMap, debounceTime, distinctUntilChanged, catchError } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
  selector: 'mt-empresas',
  templateUrl: './empresas.component.html',
  animations: [
    trigger('toggleSearch', [
      state('hidden', style({
        opacity: 0,
        "max-height": "0px"
      })),
      state('visible', style({
        opacity: 1,
        "max-height": "70px",
        "margin-top": "20px"
      })),
      transition('* => *', animate('250ms 0s ease-in-out'))
    ])
  ]
})
export class EmpresasComponent implements OnInit {

  searchBarState = 'hidden'

  empresas: Empresa[]

  searchForm: FormGroup
  searchControl: FormControl;

  constructor(private empresasService: EmpresasService, private fb: FormBuilder) { }

  ngOnInit() {

    this.searchControl = this.fb.control('');

    this.searchForm = this.fb.group({
      searchControl: this.searchControl
    })

    this.searchControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(
          searchTerm =>
            this.empresasService
              .empresas(searchTerm)
              .pipe(catchError(error => from([]))))
      ).subscribe(empresas => this.empresas = empresas)

    this.empresasService.empresas()
      .subscribe(empresas => this.empresas = empresas)
  }

  toggleSearch() {
    this.searchBarState = this.searchBarState === 'hidden' ? 'visible' : 'hidden'
  }

}
