import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Empresa } from './empresa.model';
import { ControleService } from '../../painel-controle/controle.service';


@Component({
  selector: 'mt-empresa',
  templateUrl: './empresa.component.html'
})
export class EmpresaComponent implements OnInit {

  @Input() empresa: Empresa
  @Output() add = new EventEmitter()
  constructor(private controleService: ControleService) { }

  ngOnInit() {
  }

  emitAddEvent() {
    this.add.emit(this.empresa)
  }

  addItem(item: any) {
    this.controleService.addItem(item)
  }
}