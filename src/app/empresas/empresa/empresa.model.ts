export interface Empresa {

  id: string
  nomeRazao: string
  CNPJ: string
  email?: string
  numero?: number
  dias?: string
  dataLiberado?: Date
  dataBloqueio?: Date
  imagePath?: string
}