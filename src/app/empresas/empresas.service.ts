import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs'

import { Empresa } from "./empresa/empresa.model"
import { MenuItem } from '../empresa-detail/menu-item/menu-item.model';

import { MEAT_API } from '../app.api'


@Injectable()
export class EmpresasService {

    constructor(private http: HttpClient) { }

    empresas(search?: string): Observable<Empresa[]> {
        let params: HttpParams = undefined
        if (search) {
            params = new HttpParams().set('q', search)
        }

        return this.http.get<Empresa[]>(`${MEAT_API}/empresas`, { params: params })
    }

    empresaById(id: string): Observable<Empresa> {
        return this.http.get<Empresa>(`${MEAT_API}/empresas/${id}`)
    }

    reviewsOfEmpresa(id: string): Observable<any> {
        return this.http.get(`${MEAT_API}/empresas/${id}/reviews`)
    }

    menuOfEmpresa(id: string): Observable<MenuItem[]> {
        return this.http.get<MenuItem[]>(`${MEAT_API}/empresas/${id}/menu`)
    }
}