import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
    OwlTooltipModule, OwlInputMaskModule, OWL_DIALOG_CONFIG,
    OwlDialogModule, OwlFormFieldModule, OwlInputModule, OwlRippleModule,
} from 'owl-ng';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { BottonComponent } from './bottom/bottom.component';
import { InputComponent } from "./input/input.component";
import { RadioComponent } from "./radio/radio.component";
import { RatingComponent } from "./rating/rating.component";
import { SnackbarComponent } from "./messages/snackbar/snackbar.component";
import { LoggedInGuarda } from '../security/loggedin.guarda';

import { OwlInputComponent } from './input/owl/owl.component';
import { DialogComponent } from './dialog/dialog.component';
import { BasicComponent } from './dialog/basic/basic.component';
import { DummyDialogComponent } from './dialog/dummy-dialog/dummy-dialog.component';

import { EmpresasService } from "../empresas/empresas.service";
import { CadastroService } from "../cadastro/cadastro.service";
import { NotificationService } from "./messages/notification.service";
import { LoginService } from '../security/login/login.service';

@NgModule({
    declarations: [
        BottonComponent,
        DialogComponent,
        InputComponent,
        OwlInputComponent,
        RadioComponent,
        RatingComponent,
        SnackbarComponent,
        BasicComponent,
        DummyDialogComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        OwlDialogModule,
        OwlRippleModule,
        OwlFormFieldModule,
        OwlInputModule,
        OwlTooltipModule,
        OwlInputMaskModule,
    ],
    exports: [
        BottonComponent,
        DialogComponent,
        InputComponent,
        OwlInputComponent,
        RadioComponent,
        RatingComponent,
        SnackbarComponent,
        BasicComponent,
        DummyDialogComponent,

        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [
        {
            // Global Dialog Config
            // the config would apply to all the dialogs under this module
            provide: OWL_DIALOG_CONFIG, useValue: {
                dialogClass: 'dummy-dialog',
                width: '400px',
            }
        }
    ],
    bootstrap: [BasicComponent],
    entryComponents: [DummyDialogComponent]
})


export class SharedModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                EmpresasService,
                CadastroService,
                NotificationService,
                LoginService,
                LoggedInGuarda,
            ]

        }
    }
}