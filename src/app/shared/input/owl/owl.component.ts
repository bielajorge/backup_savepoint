import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'owl-input',
    templateUrl: './owl.component.html',
})
export class OwlInputComponent implements OnInit {

    diffDays: any;

    public date
    public min = new Date();

    constructor() {
    }

    getDias(data: any) {

        var date1 = new Date();
        var date2 = new Date(data);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        this.diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

        if (this.diffDays > 0)
            return this.diffDays
        else
            return "?"
    }

    ngOnInit() {
        var outraData = new Date();
        outraData.setDate(outraData.getDate() + 30);
        this.date = new Date(outraData);
    }

    increaseQty(data: any) {

        var time = new Date(data);
        var outraData = new Date(this.date);
        outraData.setDate(time.getDate() + 30);

        this.date = new Date(outraData)

    }

    decreaseQty(data: any) {
        var time = new Date(data);
        var outraData = new Date(this.date);
        outraData.setDate(time.getDate() - 30);


        var today = new Date()
        if ((today.getMonth() <= outraData.getMonth()) || (today.getFullYear() < outraData.getFullYear())) {
            this.date = new Date(outraData)
        }
    }

    remove() {
        this.date = undefined
    }



}


