import { EventEmitter } from '@angular/core';

export class NotificationService {
    notifier = new EventEmitter<string>()
    tipo = new EventEmitter<number>()
    
    notify(message: string) {
        this.notifier.emit(message)
    }

}