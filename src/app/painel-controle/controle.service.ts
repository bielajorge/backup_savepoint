import { Injectable } from "@angular/core";

import { ControleItem } from "./controle.model";
import { NotificationService } from "../shared/messages/notification.service";
import { Empresa } from "../empresas/empresa/empresa.model";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ControleService {

    empresas: ControleItem[] = []

    private customersUrl = 'http://localhost:8080/api/customers';  // URL to web api

    constructor(
        private notificationService: NotificationService,
        private http: HttpClient) { }

    clear() {
        this.empresas = []
    }

    increaseQty(item: ControleItem) {
        item.quantity = item.quantity + 1
    }

    decreaseQty(item: ControleItem) {
        item.quantity = item.quantity - 1
        if (item.quantity === 0)
            this.removeItem(item)
    }

    addItem(empresa: Empresa) {
        let foundItem = this.empresas.find((mItem) => mItem.menuItem.id === empresa.id)
        if (!foundItem) {
            this.empresas.push(new ControleItem(empresa))
            this.notificationService.notify(`Você selecionou ${empresa.nomeRazao}`)
        }
    }

    removeItem(item: ControleItem) {
        this.empresas.splice(this.empresas.indexOf(item), 1)
        this.notificationService.notify(`Você removeu ${item.menuItem.nomeRazao}`)
    }

    liberar() {
        this.empresas
        this.notificationService.notify(`Empresa(s) liberada(s)`)
    }

    total(): number {
        return 0
    }

    /*
    addCustomer(empresa: Empresa): Observable<Empresa> {
        return this.http.post<Empresa>(this.customersUrl, empresa, httpOptions);
    }*/

}