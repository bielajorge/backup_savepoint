import { Component, OnInit, Input } from '@angular/core';

import { ControleService } from '../painel-controle/controle.service';
import { Empresa } from '../empresas/empresa/empresa.model';


@Component({
  selector: 'mt-control',
  templateUrl: './controle.component.html'
})
export class ControleComponent implements OnInit {


  submitted = false;

  @Input() empresa: Empresa

  location: any;

  constructor(
    private controlService: ControleService) { }

  ngOnInit() {
  }

  empresas(): any[] {
    return this.controlService.empresas;
  }

  total(): number {
    return this.controlService.total()
  }

  clear() {
    this.controlService.clear()
  }

  removeItem(item: any) {
    this.controlService.removeItem(item)
  }

  addItem(item: any) {
    this.controlService.addItem(item)
  }

  liberar() {
    this.controlService.liberar()
  }

  addCustomer() {
    this.submitted = true;
    //this.save();
  }

  /*
  goBack(): void {
    this.location.back();
  }

  private save(): void {
    console.log(this.empresa);
    this.controlService.addCustomer(this.empresa)
      .subscribe();
  }*/



}
