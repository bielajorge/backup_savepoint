import { Routes } from "@angular/router";
import { InicioComponent } from "./inicio/inicio.component";
import { EmpresasComponent } from "./empresas/empresas.component";
import { RestaurantsDetailComponent } from "./empresa-detail/restaurants-detail.component";
import { ReviewsComponent } from "./empresa-detail/reviews/reviews.component";
import { CadastroComponent } from "./cadastro/cadastro.component";
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './security/login/login.component';

export const ROUTES: Routes = [
    { path: '', component: InicioComponent },
    { path: 'home', component: InicioComponent },
    { path: 'login', component: LoginComponent },
    { path: 'empresas', component: EmpresasComponent },
    {
        path: 'empresas/:id', component: RestaurantsDetailComponent,
        children: [
            { path: '', redirectTo: 'reviews', pathMatch: 'full' },
            { path: 'reviews', component: ReviewsComponent }
        ]
    },
    { path: 'cadastro', component: CadastroComponent },
    { path: '**', component: NotFoundComponent }

]  